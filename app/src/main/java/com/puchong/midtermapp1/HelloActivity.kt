package com.puchong.midtermapp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val textShow = intent.getStringExtra(EXTRA_MESSAGE)

        val textview = findViewById<TextView>(R.id.name).apply {
            text = textShow
        }
    }
}