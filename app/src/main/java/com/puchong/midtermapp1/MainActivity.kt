package com.puchong.midtermapp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.TextView
import com.puchong.midtermapp1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myName: TextView = findViewById(R.id.my_name)
        val myID: TextView = findViewById(R.id.my_id)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button.setOnClickListener {
            Log.d(TAG, ""+myName.text)
            Log.d(TAG, ""+myID.text)
            val textShow = myName.text
            val intent = Intent(this,HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, textShow)
            }
            startActivity(intent)
        }

    }

    companion object{
        private const val TAG = "MainActivity"
    }
}